package Steps;

import Models.CompanyModel;
import Models.EmployeeModel;
import Page.CompanySearchPage;
import Page.CreateCompanyPage;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class CompanyAddAndSearchSteps {
    public String addCategory;

    @Step("Поиск компании по названию")
    public void searchCompanyByNameAndOpen(CompanyModel companyModel) {
        $(new CompanySearchPage().onCompanySearchForm().searchButton).shouldBe(visible);
        $(new CompanySearchPage().onCompanySearchForm().nameSubstringFilter).shouldBe(visible).click();
        $(new CompanySearchPage().onCompanySearchForm().nameSubstringFilterInput).sendKeys(companyModel.getName());
        $(new CompanySearchPage().onCompanySearchForm().getCompanyName(companyModel.getName())).click();
    }

    @Step("Поиск компании по названию")
    public void searchCompanyByName(CompanyModel companyModel) {
        $(new CompanySearchPage().onCompanySearchForm().searchButton).shouldBe(visible);
        $(new CompanySearchPage().onCompanySearchForm().nameSubstringFilter).shouldBe(visible).click();
        $(new CompanySearchPage().onCompanySearchForm().nameSubstringFilterInput).sendKeys(companyModel.getName());
        $(new CompanySearchPage().onCompanySearchForm().searchButton).click();
    }

    @Step("Поиск компании по ФИО сотрудника")
    public void searchCompanyByNameEmployee(CompanyModel companyModel, EmployeeModel employeeModel) {
        $(new CompanySearchPage().onCompanySearchForm().nameEmployeeSubstringFilter).shouldBe(visible);
        $(new CompanySearchPage().onCompanySearchForm().nameEmployeeSubstringFilter).sendKeys(employeeModel.getNameEmployees());
        $(new CompanySearchPage().onCompanySearchForm().phoneEmployeeSubstringFilter).sendKeys(employeeModel.getPhoneEmployees());
        $(new CompanySearchPage().onCompanySearchForm().searchButton).click();
        $(new CompanySearchPage().onCompanySearchForm().getCompanyName(companyModel.getName())).shouldBe(visible);
    }

    @Step("Очистка данных с полей параметров поиска")
    public void clearFilter() {
        $(new CompanySearchPage().onCompanySearchForm().clearButton).click();
        $(new CompanySearchPage().onCompanySearchForm().nameEmployeeSubstringFilter).shouldBe();
        $(new CompanySearchPage().onCompanySearchForm().phoneEmployeeSubstringFilter).shouldBe();
    }

    @Step("Добавить компанию от ЮЛ")
    public void createCompanyYL(CompanyModel companyModel) {
        $(new CreateCompanyPage().onCompanyFormCreate().businessRegistrationLabel).shouldHave(exactText("Юридическое лицо"));
        $(new CreateCompanyPage().onCompanyFormCreate().companyAddButton).click();
        $(new CreateCompanyPage().onCompanyFormCreate().fullCompanyNameInput).sendKeys(companyModel.getFullName());
        $(new CreateCompanyPage().onCompanyFormCreate().companyNameInput).sendKeys(companyModel.getName());
        $(new CreateCompanyPage().onCompanyFormCreate().innInput).sendKeys(companyModel.getInn());
        $(new CreateCompanyPage().onCompanyFormCreate().companyAddressInput).sendKeys(companyModel.getAddress());
        $(new CreateCompanyPage().onCompanyFormCreate().categoryIdLabel).click();
        addCategory = $(new CreateCompanyPage().onCompanyFormCreate().categoryAddButton).getText();
        $(new CreateCompanyPage().onCompanyFormCreate().categoryAddButton).click();
        $(new CreateCompanyPage().onCompanyFormCreate().commissionInput).sendKeys(companyModel.getCommission());
        $(new CreateCompanyPage().onCompanyFormCreate().companyCreateButton).click();
    }
}
