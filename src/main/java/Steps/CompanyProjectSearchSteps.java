package Steps;

import Page.CompanyCardPage;
import io.qameta.allure.Step;


import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class CompanyProjectSearchSteps {
    @Step("Поиск проекта компании по наименованию")
    public void searchCompanyProjectByName() {
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().companyProjectAndObjectButton).shouldBe(visible).click();
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().nameCompanyProjectIn).shouldBe(visible);
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().nameCompanyProjectIn).sendKeys("Crown");
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().searchButton).click();
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().nameCompanyProjectLabel).shouldHave(exactText("Crown"));
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().clearButton).click();
    }

    @Step("Поиск проекта компании по статусу проекта")
    public void searchCompanyProjectByStatus() {
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().statusCompanyProjectFilter).click();
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().statusCompanyProjectFilterLabel).shouldBe(visible).click();
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().searchButton).shouldBe(visible).click();
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().statusCompanyProjectLabel).shouldHave(exactText("Закрыт"));
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().clearButton).shouldBe(visible).click();
    }

    @Step("Очистка данных с полей параметров поиска")
    public void clearFilter() {
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().clearButton).click();
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().nameCompanyProjectIn).shouldBe();
        $(new CompanyCardPage().onCompanyProjectAndObjectForm().statusCompanyProjectFilter).shouldHave(exactText("Все"));
    }

}
