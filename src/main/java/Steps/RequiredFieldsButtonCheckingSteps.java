package Steps;

import Page.RequiredFieldsButtonCheckingPage;
import io.qameta.allure.Step;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class RequiredFieldsButtonCheckingSteps {
    @Step("Нажать кнопку 'Добавить'")
    public void checkingButtonRequiredFields() {
        $(new RequiredFieldsButtonCheckingPage().onCheckRequiredFieldsButtonForms().companyCreateButton).shouldBe(visible).click();
        $(new RequiredFieldsButtonCheckingPage().onCheckRequiredFieldsButtonForms().companyAddButton).shouldBe(visible).click();
    }

    @Step("Проверка сообщения об обязательных полях заполнения")
    public void checkingRequiredFields() {
        $(new RequiredFieldsButtonCheckingPage().onCheckRequiredFieldsButtonForms().requiredFieldForCompanyNameLabel).shouldHave(exactText("Обязательное поле"));
        $(new RequiredFieldsButtonCheckingPage().onCheckRequiredFieldsButtonForms().requiredFieldForShortCompanyNameLabel).shouldHave(exactText("Обязательное поле"));
        $(new RequiredFieldsButtonCheckingPage().onCheckRequiredFieldsButtonForms().requiredFieldForINNLabel).shouldHave(exactText("Обязательное поле"));
        $(new RequiredFieldsButtonCheckingPage().onCheckRequiredFieldsButtonForms().requiredFieldForAddressLabel).shouldHave(exactText("Обязательное поле"));
        $(new RequiredFieldsButtonCheckingPage().onCheckRequiredFieldsButtonForms().requiredFieldForCategoryLabel).shouldHave(exactText("Обязательное поле"));
    }
}
