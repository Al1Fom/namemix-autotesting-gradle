package Steps;

import com.codeborne.selenide.Configuration;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.open;

public class MainPageSteps {

    public void openMainPage(){
        Configuration.startMaximized = true;
        open(System.getenv("URL"));
    }
    public void closeMainPage(){
        closeWebDriver();
    }
}
