package Steps;

import Models.*;
import Page.CompanyCardPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

import Page.CompanyDocumentsPage;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

public class CompanySteps {

    @Step("Проверить параметры компании")
    public void validateParamsCompany(CompanyModel companyModel, String addCategory) {
        $(new CompanyCardPage().onCompanyCardForm().companyNameLabel).shouldBe(visible);
        $(new CompanyCardPage().onCompanyCardForm().companyNameLabel).shouldHave(text(companyModel.getName()));
        $(new CompanyCardPage().onCompanyCardForm().companyFullNameLabel).shouldHave(text(companyModel.getFullName()));
        $(new CompanyCardPage().onCompanyCardForm().companyInnLabel).shouldHave(text(companyModel.getInn()));
        $(new CompanyCardPage().onCompanyCardForm().companyAddressLabel).shouldHave(text(companyModel.getAddress()));
        $(new CompanyCardPage().onCompanyCardForm().companyCategoryLabel).shouldHave(text(addCategory));
    }

    @Step("Добавление сотрудника компании")
    public void addEmployees(EmployeeModel employeeModel) {
        $(new CompanyCardPage().onCompanyCardForm().employeesButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().addEmployeesButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().lastNameEmployeesInput).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().lastNameEmployeesInput).sendKeys(employeeModel.getLastNameEmployees());
        $(new CompanyCardPage().onCompanyCardForm().firstNameEmployeesInput).sendKeys(employeeModel.getFirstNameEmployees());
        $(new CompanyCardPage().onCompanyCardForm().patronymicEmployeesInput).sendKeys(employeeModel.getPatronymicEmployees());
        $(new CompanyCardPage().onCompanyCardForm().snilsEmployeesInput).sendKeys(employeeModel.getSnilsEmployees());
        $(new CompanyCardPage().onCompanyCardForm().innEmployeesInput).sendKeys(employeeModel.getInnEmployees());
        $(new CompanyCardPage().onCompanyCardForm().positionEmployeesButton).click();
        $(new CompanyCardPage().onCompanyCardForm().positionLeadershipButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().roleEmployeesButton).click();
        $(new CompanyCardPage().onCompanyCardForm().roleLeadershipButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().phoneEmployeesInput).sendKeys(employeeModel.getPhoneEmployees());
        $(new CompanyCardPage().onCompanyCardForm().emailEmployeesInput).sendKeys(employeeModel.getEmailEmployees());
        $(new CompanyCardPage().onCompanyCardForm().passwordEmployeesInput).sendKeys(employeeModel.getPasswordEmployees());
        $(new CompanyCardPage().onCompanyCardForm().repeatPasswordEmployeesInput).sendKeys(employeeModel.getPasswordEmployees());
        $(new CompanyCardPage().onCompanyCardForm().applyButton).click();
    }

    @Step("Проверка добавленного клиента")
    public void checkAddEmployees(EmployeeModel employeeModel) {
        $(new CompanyCardPage().onCompanyCardForm().nameEmployeesLabel).shouldHave(text(employeeModel.getNameEmployees()));
        $(new CompanyCardPage().onCompanyCardForm().positionEmployeesLabel).shouldHave(text(employeeModel.getPositionEmployees()));
        $(new CompanyCardPage().onCompanyCardForm().phoneEmployees).shouldHave(text(employeeModel.getPhoneEmployeesLabel()));
        $(new CompanyCardPage().onCompanyCardForm().emailEmployees).shouldHave(text(employeeModel.getEmailEmployees()));
    }

    @Step("Удаление Сотрудника")
    public void deleteEmployees() {
        $(new CompanyCardPage().onCompanyCardForm().deleteEmployeesButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().proofDeleteEmployeesButton).shouldHave(visible).click();
    }

    @Step("Редактирование карточки Банковские реквизиты")
    public void editCompanyBankDetails(CompanyBankDetailsModel companyBankDetailsModel) {
        $(new CompanyCardPage().onCompanyCardForm().editBankDetailsButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().bankNameInput).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().bankNameInput).sendKeys(companyBankDetailsModel.getBankName());
        $(new CompanyCardPage().onCompanyCardForm().bankAddressInput).sendKeys(companyBankDetailsModel.getBankAddress());
        $(new CompanyCardPage().onCompanyCardForm().bankBicInput).sendKeys(companyBankDetailsModel.getBankBic());
        $(new CompanyCardPage().onCompanyCardForm().bankCheckAccountInput).sendKeys(companyBankDetailsModel.getBankCheckAccount());
        $(new CompanyCardPage().onCompanyCardForm().bankCorrAccountInput).sendKeys(companyBankDetailsModel.getBankCorrAccount());
        $(new CompanyCardPage().onCompanyCardForm().applyEditBankDetailsButton).click();
    }

    @Step("Проверка редактирования Банковские реквизиты")
    public void checkCompanyBankDetails(CompanyBankDetailsModel companyBankDetailsModel) {
        $(new CompanyCardPage().onCompanyCardForm().bankNameLabel).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().bankNameLabel).shouldHave(text(companyBankDetailsModel.getBankName()));
        $(new CompanyCardPage().onCompanyCardForm().bankAddressLabel).shouldHave(text(companyBankDetailsModel.getBankAddress()));
        $(new CompanyCardPage().onCompanyCardForm().bankBicLabel).shouldHave(text(companyBankDetailsModel.getBankBic()));
        $(new CompanyCardPage().onCompanyCardForm().bankCheckAccountLabel).shouldHave(text(companyBankDetailsModel.getBankCheckAccount()));
        $(new CompanyCardPage().onCompanyCardForm().bankCorrAccountLabel).shouldHave(text(companyBankDetailsModel.getBankCorrAccount()));
    }

    @Step("Проверка обязательных полей при редактировании карточки Банковский представитель")
    public void checkingRequiredFieldCompanyBankRepresentative() {
        $(new CompanyCardPage().onCompanyCardForm().editNameRepresentativeButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().applyNameRepresentativeButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldForNameRepresentativeLabel).shouldHave(text("Обязательное поле"));
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldForBasedOnLabel).shouldHave(text("Обязательное поле"));
    }

    @Step("Проверка обязательных полей при редактировании карточки Банковский представитель")
    public void editCompanyBankRepresentative(CompanyBankRepresentativeModel companyBankRepresentativeModel) {
        $(new CompanyCardPage().onCompanyCardForm().editNameRepresentativeButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().nameRepresentativeInput).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().nameRepresentativeInput).sendKeys(companyBankRepresentativeModel.getName());
        $(new CompanyCardPage().onCompanyCardForm().basedOnInput).sendKeys(companyBankRepresentativeModel.getBasedOn());
        $(new CompanyCardPage().onCompanyCardForm().applyNameRepresentativeButton).click();
    }

    @Step("Проверка изменения карточки Банковский представитель")
    public void checkCompanyBankRepresentative(CompanyBankRepresentativeModel companyBankRepresentativeModel) {
        $(new CompanyCardPage().onCompanyCardForm().nameRepresentativeLabel).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().nameRepresentativeLabel).shouldHave(text(companyBankRepresentativeModel.getName()));
        $(new CompanyCardPage().onCompanyCardForm().basedOnLabel).shouldHave(text(companyBankRepresentativeModel.getBasedOn()));
    }

    @Step("Редактирование карточки компании - Реквизиты компании-Проверка заполнения обязательных полей")
    public void updateInformationCompanyCardCheckRequiredFields() {
        $(new CompanyCardPage().onCompanyCardForm().editButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().cancelButton).shouldBe(visible);
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        $(new CompanyCardPage().onCompanyCardForm().nameIPInput).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        $(new CompanyCardPage().onCompanyCardForm().addressInput).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        $(new CompanyCardPage().onCompanyCardForm().innInput).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        $(new CompanyCardPage().onCompanyCardForm().cancelButton).click();
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldFullNameLabel).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldFullNameLabel).shouldHave(text("Обязательное поле"));
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldNameLabel).shouldHave(text("Обязательное поле"));
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldAddressLabel).shouldHave(text("Обязательное поле"));
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldInnLabel).shouldHave(text("Обязательное поле"));
        $(new CompanyCardPage().onCompanyCardForm().cancelButton).click();
    }

    @Step("Изменение сокращенного названия компании-Проверка валидации поля 'Сокращенное название компании'")
    public void updateInformationCompanyNameCard() {
        $(new CompanyCardPage().onCompanyCardForm().editButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().cancelButton).shouldBe(visible);
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).sendKeys("test");
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldNameLabel).shouldHave(text("Минимальная длина строки 5 символов"));
    }

    @Step("Изменение сокращенного названия компании")
    public void updateInformationCompanyCard() {
        $(new CompanyCardPage().onCompanyCardForm().editButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).sendKeys("test IP company");
        $(new CompanyCardPage().onCompanyCardForm().requiredFieldUpdateNameLabel).shouldHave(text("test IP company"));
    }

    @Step("Отмена изменения сокращенного названия компании")
    public void cancelUpdateInformationCompanyCard() {
        $(new CompanyCardPage().onCompanyCardForm().editButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        $(new CompanyCardPage().onCompanyCardForm().fullNameIPInput).sendKeys("test IP company");
        $(new CompanyCardPage().onCompanyCardForm().cancelButton).shouldBe(visible).click();
    }

    @Step("Редактирование карточку Главный бухгалтер")
    public void editCompanyChief(CompanyChiefModel companyChiefModel) {
        $(new CompanyCardPage().onCompanyCardForm().editCompanyChiefButton).shouldHave(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().editNameCompanyChiefInput).shouldHave(visible).sendKeys(companyChiefModel.getNameChief());
        $(new CompanyCardPage().onCompanyCardForm().editPhoneCompanyChiefInput).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        $(new CompanyCardPage().onCompanyCardForm().editPhoneCompanyChiefInput).sendKeys(companyChiefModel.getChiefPhone());
        $(new CompanyCardPage().onCompanyCardForm().applyEditCompanyChiefButton).click();
    }

    @Step("Проверка изменения карточки Главный бухгалтер")
    public void checkCompanyChief(CompanyChiefModel companyChiefModel) {
        $(new CompanyCardPage().onCompanyCardForm().nameCompanyChiefLabel).shouldHave(visible);
        $(new CompanyCardPage().onCompanyCardForm().nameCompanyChiefLabel).shouldHave(text(companyChiefModel.getNameChief()));
        $(new CompanyCardPage().onCompanyCardForm().phoneCompanyChiefLabel).shouldHave(text(companyChiefModel.getUpdatePhone()));
    }

    @Step("Редактировать карточку Руководитель компании")
    public void editCompanyManager(CompanyManagerModel companyManagerModel) {
        $(new CompanyCardPage().onCompanyCardForm().editCompanyManagerButton).shouldBe(visible).click();
        $(new CompanyCardPage().onCompanyCardForm().editNameCompanyManagerInput).shouldBe(visible);
        $(new CompanyCardPage().onCompanyCardForm().editNameCompanyManagerInput).sendKeys(companyManagerModel.getNameManager());
        $(new CompanyCardPage().onCompanyCardForm().editPhoneCompanyManagerInput).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        $(new CompanyCardPage().onCompanyCardForm().editPhoneCompanyManagerInput).sendKeys(companyManagerModel.getManagerPhone());
        $(new CompanyCardPage().onCompanyCardForm().applyEditCompanyManagerButton).click();
    }

    @Step("Проверка изменения карточки Руководителя компании")
    public void checkEditCompanyManager(CompanyManagerModel companyManagerModel) {
        $(new CompanyCardPage().onCompanyCardForm().nameCompanyManagerLabel).shouldBe(visible);
        $(new CompanyCardPage().onCompanyCardForm().nameCompanyManagerLabel).shouldHave(text(companyManagerModel.getNameManager()));
        $(new CompanyCardPage().onCompanyCardForm().phoneCompanyManagerLabel).shouldHave(text(companyManagerModel.getUpdateManagerPhone()));
    }

    @Step("Добавление информации в Агентский договор")
    public void addingInformationToAgencyAgreement() {
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().documentsButton).shouldBe(visible).click();
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().agencyAgreementButton).shouldBe(visible).click();
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().editButton).shouldBe(visible).click();
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().numberAgencyAgreementInput).shouldBe(visible).sendKeys("123");
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().dateAgencyAgreementInput).click();
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().updateDateButton).shouldBe(visible).click();
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().updateInformationButton).click();
    }

    @Step("Проверка полей на изменение своих значения на введенные")
    public void checkingAddedInformation() {
        $(new CompanyDocumentsPage().onCompanyDocumentsForm().numberAgencyAgreementLabel).shouldHave(text("123"));
    }
}
