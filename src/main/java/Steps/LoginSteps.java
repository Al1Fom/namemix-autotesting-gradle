package Steps;

import Page.LoginPage;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;


public class LoginSteps {
    @Step("Авторизация")
    public void authorization() {
        $(new LoginPage().onAuthorizationForm().loginInputIn).shouldBe(visible).sendKeys(System.getenv("LOGIN"));
        $(new LoginPage().onAuthorizationForm().passwordInputIn).sendKeys(System.getenv("PASSWORD"));
        $(new LoginPage().onAuthorizationForm().signInButton).click();
    }
}
