package Models;

public class CompanyManagerModel {
    String managerPhone;
    String nameManager;
    String updateManagerPhone;

    public String getManagerPhone() {
        return managerPhone;
    }

    public CompanyManagerModel setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
        return this;
    }

    public String getNameManager() {
        return nameManager;
    }

    public CompanyManagerModel setNameManager(String nameManager) {
        this.nameManager = nameManager;
        return this;
    }

    public String getUpdateManagerPhone() {
        return updateManagerPhone;
    }

    public CompanyManagerModel setUpdateManagerPhone(String updateManagerPhone) {
        this.updateManagerPhone = updateManagerPhone;
        return this;
    }

}
