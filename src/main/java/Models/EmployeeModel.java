package Models;

public class EmployeeModel {
    String lastNameEmployees;
    String firstNameEmployees;
    String patronymicEmployees;
    String snilsEmployees;
    String innEmployees;
    String phoneEmployees;
    String phoneEmployeesLabel;
    String emailEmployees;
    String passwordEmployees;
    String repeatPasswordEmployees;
    String positionEmployees;

    public String getLastNameEmployees() {
        return lastNameEmployees;
    }

    public String getFirstNameEmployees() {
        return firstNameEmployees;
    }

    public String getPatronymicEmployees() {
        return patronymicEmployees;
    }

    public String getNameEmployees() {
        return lastNameEmployees + " " + firstNameEmployees + " " + patronymicEmployees;
    }

    public String getSnilsEmployees() {
        return snilsEmployees;
    }

    public String getInnEmployees() {
        return innEmployees;
    }

    public String getPhoneEmployees() {
        return phoneEmployees;
    }

    public String getPhoneEmployeesLabel() {
        return phoneEmployeesLabel;
    }

    public String getEmailEmployees() {
        return emailEmployees;
    }

    public String getPasswordEmployees() {
        return passwordEmployees;
    }

    public String getRepeatPasswordEmployees() {
        return repeatPasswordEmployees;
    }

    public String getPositionEmployees() {
        return positionEmployees;
    }

    public EmployeeModel setLastNameEmployees(String lastNameEmployees) {
        this.lastNameEmployees = lastNameEmployees;
        return this;
    }

    public EmployeeModel setFirstNameEmployees(String firstNameEmployees) {
        this.firstNameEmployees = firstNameEmployees;
        return this;
    }

    public EmployeeModel setPatronymicEmployees(String patronymicEmployees) {
        this.patronymicEmployees = patronymicEmployees;
        return this;
    }

    public EmployeeModel setSnilsEmployees(String snilsEmployees) {
        this.snilsEmployees = snilsEmployees;
        return this;
    }

    public EmployeeModel setInnEmployees(String innEmployees) {
        this.innEmployees = innEmployees;
        return this;
    }

    public EmployeeModel setPhoneEmployees(String phoneEmployees) {
        this.phoneEmployees = phoneEmployees;
        return this;
    }

    public EmployeeModel setPhoneEmployeesLabel(String phoneEmployeesLabel) {
        this.phoneEmployeesLabel = phoneEmployeesLabel;
        return this;
    }

    public EmployeeModel setEmailEmployees(String emailEmployees) {
        this.emailEmployees = emailEmployees;
        return this;
    }

    public EmployeeModel setPasswordEmployees(String passwordEmployees) {
        this.passwordEmployees = passwordEmployees;
        return this;
    }

    public EmployeeModel setRepeatPasswordEmployees(String repeatPasswordEmployees) {
        this.repeatPasswordEmployees = repeatPasswordEmployees;
        return this;
    }

    public EmployeeModel setPositionEmployees(String positionEmployees) {
        this.positionEmployees = positionEmployees;
        return this;
    }
}
