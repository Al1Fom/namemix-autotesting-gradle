package Models;

public class CompanyChiefModel {
    String nameChief;
    String chiefPhone;

    public String getUpdatePhone() {
        return updatePhone;
    }

    public CompanyChiefModel setUpdatePhone(String updatePhone) {
        this.updatePhone = updatePhone;
        return this;
    }

    String updatePhone;

    public String getNameChief() {
        return nameChief;
    }

    public CompanyChiefModel setNameChief(String nameChief) {
        this.nameChief = nameChief;
        return this;
    }

    public String getChiefPhone() {
        return chiefPhone;
    }

    public CompanyChiefModel setChiefPhone(String chiefPhone) {
        this.chiefPhone = chiefPhone;
        return this;
    }

}
