package Models;

public class CompanyBankDetailsModel {
    String bankName;
    String bankAddress;
    String bankBic;
    String bankCheckAccount;
    String bankCorrAccount;

    public String getBankName() {
        return bankName;
    }

    public CompanyBankDetailsModel setBankName(String bankName) {
        this.bankName = bankName;
        return this;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public CompanyBankDetailsModel setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
        return this;
    }

    public String getBankBic() {
        return bankBic;
    }

    public CompanyBankDetailsModel setBankBic(String bankBic) {
        this.bankBic = bankBic;
        return this;
    }

    public String getBankCheckAccount() {
        return bankCheckAccount;
    }

    public CompanyBankDetailsModel setBankCheckAccount(String bankCheckAccount) {
        this.bankCheckAccount = bankCheckAccount;
        return this;
    }

    public String getBankCorrAccount() {
        return bankCorrAccount;
    }

    public CompanyBankDetailsModel setBankCorrAccount(String bankCorrAccount) {
        this.bankCorrAccount = bankCorrAccount;
        return this;
    }
}
