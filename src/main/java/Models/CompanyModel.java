package Models;


public class CompanyModel {
    String name;
    String fullName;
    String inn;
    String address;

    public String getCommission() {
        return commission;
    }

    public CompanyModel setCommission(String commission) {
        this.commission = commission;
        return this;
    }

    String commission;

    public String getName() {
        return name;
    }

    public CompanyModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public CompanyModel setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getInn() {
        return inn;
    }

    public CompanyModel setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public CompanyModel setAddress(String address) {
        this.address = address;
        return this;
    }

}
