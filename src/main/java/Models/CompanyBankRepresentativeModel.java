package Models;

public class CompanyBankRepresentativeModel {
    String name;
    String basedOn;

    public String getName() {
        return name;
    }

    public CompanyBankRepresentativeModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getBasedOn() {
        return basedOn;
    }

    public CompanyBankRepresentativeModel setBasedOn(String basedOn) {
        this.basedOn = basedOn;
        return this;
    }
}
