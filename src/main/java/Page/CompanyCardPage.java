package Page;

import Forms.CompanyCardForm;
import Forms.CompanyProjectAndObjectForm;

public class CompanyCardPage {
    public CompanyCardForm onCompanyCardForm() {
        return new CompanyCardForm();
    }

    public CompanyProjectAndObjectForm onCompanyProjectAndObjectForm() {
        return new CompanyProjectAndObjectForm();
    }
}
