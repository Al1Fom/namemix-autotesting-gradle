package Forms;

import org.openqa.selenium.By;

public class CompanyCardForm {
    public By companyNameLabel = By.cssSelector(".col-xl-5 > .card-app:nth-child(1) > .label-text:nth-child(3) > .label-text__content");
    public By companyFullNameLabel = By.cssSelector(".col-xl-5 > .card-app:nth-child(1) > .label-text:nth-child(2) > .label-text__content");
    public By companyInnLabel = By.cssSelector(".label-text:nth-child(15) > .label-text__content");
    public By companyAddressLabel = By.cssSelector(".label-text:nth-child(9) > .label-text__content");
    public By companyCategoryLabel = By.cssSelector(".col-xl-5 > .card-app:nth-child(3) > .label-text:nth-child(3) > .label-text__content");
    public By editButton = By.cssSelector(".col-xl-5 > .card-app:nth-child(1) .nmx-icon");
    public By cancelButton = By.cssSelector(".card-app__icon-check");
    public By fullNameIPInput = By.xpath("//input[@name='fullName']");
    public By nameIPInput = By.xpath("//input[@name='name']");
    public By addressInput = By.cssSelector(".react-dadata__input");
    public By innInput = By.xpath("//input[@name='inn']");
    public By requiredFieldFullNameLabel = By.cssSelector(".row:nth-child(1) .error-tooltip");
    public By requiredFieldNameLabel = By.cssSelector(".row:nth-child(2) > .col-md-16 .error-tooltip");
    public By requiredFieldAddressLabel = By.cssSelector(".nm-dadata-input > .error-tooltip");
    public By requiredFieldInnLabel = By.cssSelector(".row:nth-child(16) .error-tooltip");
    public By requiredFieldUpdateNameLabel = By.cssSelector(".col-xl-5 > .card-app:nth-child(1) > .label-text:nth-child(3) > .label-text__content");
    public By employeesButton = By.xpath("//*[text()='Сотрудники']");
    public By addEmployeesButton = By.cssSelector(".nm-button_color-green .nm-button__text");
    public By lastNameEmployeesInput = By.xpath("//input[@name='lastName']");
    public By firstNameEmployeesInput = By.xpath("//input[@name='firstName']");
    public By patronymicEmployeesInput = By.xpath("//input[@name='patronymic']");
    public By snilsEmployeesInput = By.xpath("//input[@name='snils']");
    public By innEmployeesInput = By.xpath("//input[@name='inn']");
    public By positionEmployeesButton = By.cssSelector("#position .nm-dropdown-v2__text");
    public By positionLeadershipButton = By.cssSelector(".nm-dropdown-v2__item:nth-child(2) .nm-dropdown-v2__options-text");
    public By roleEmployeesButton = By.cssSelector("#role .nm-dropdown-v2__text");
    public By roleLeadershipButton = By.cssSelector(".nm-dropdown-v2__item:nth-child(1) .nm-dropdown-v2__options-text");
    public By phoneEmployeesInput = By.xpath("//input[@name='phone']");
    public By emailEmployeesInput = By.xpath("//input[@name='email']");
    public By passwordEmployeesInput = By.xpath("//input[@name='password']");
    public By repeatPasswordEmployeesInput = By.xpath("//input[@name='repeatPassword']");
    public By applyButton = By.cssSelector(".apply-buttons__submit");
    public By nameEmployeesLabel = By.cssSelector(".left:nth-child(1)");
    public By positionEmployeesLabel = By.cssSelector(".left:nth-child(2)");
    public By phoneEmployees = By.cssSelector(".left:nth-child(3)");
    public By emailEmployees = By.cssSelector(".left:nth-child(4)");
    public By deleteEmployeesButton = By.cssSelector(".nmx-icon:nth-child(2)");
    public By proofDeleteEmployeesButton = By.cssSelector(".apply-buttons__submit .nm-button__text");
    public By editCompanyManagerButton = By.cssSelector(".col-md-8:nth-child(3) > .card-app:nth-child(1) .nmx-icon");
    public By editNameCompanyManagerInput = By.xpath("//input[@name='managerName']");
    public By editPhoneCompanyManagerInput = By.xpath("//input[@name='managerPhone']");
    public By applyEditCompanyManagerButton = By.cssSelector(".card-app__icon-check");
    public By nameCompanyManagerLabel = By.cssSelector(".card-app:nth-child(1) > .mt-15:nth-child(2) > .label-text__content");
    public By phoneCompanyManagerLabel = By.cssSelector(".mt-15:nth-child(3) > .label-text__content");
    public By editCompanyChiefButton = By.cssSelector(".col-md-8:nth-child(2) > .card-app:nth-child(2) .nmx-icon");
    public By editNameCompanyChiefInput = By.xpath("//input[@name='accountantName']");
    public By editPhoneCompanyChiefInput = By.xpath("//input[@name='accountantPhone']");
    public By applyEditCompanyChiefButton = By.cssSelector(".card-app__icon-check");
    public By nameCompanyChiefLabel = By.cssSelector(".col-md-8:nth-child(2) > .card-app:nth-child(2) > .label-text:nth-child(2) > .label-text__content");
    public By phoneCompanyChiefLabel = By.cssSelector(".col-md-8:nth-child(2) > .card-app:nth-child(2) > .label-text:nth-child(3) > .label-text__content");
    public By editBankDetailsButton = By.cssSelector(".col-md-8:nth-child(2) > .card-app:nth-child(1) .nmx-icon");
    public By bankNameInput = By.xpath("//input[@name='bankName']");
    public By bankAddressInput = By.cssSelector(".react-dadata__input");
    public By bankBicInput = By.xpath("//input[@name='bic']");
    public By bankCheckAccountInput = By.xpath("//input[@name='bankCheckAccount']");
    public By bankCorrAccountInput = By.xpath("//input[@name='bankCorrAccount']");
    public By applyEditBankDetailsButton = By.cssSelector(".card-app__icon-check");
    public By bankNameLabel = By.cssSelector(".col-md-8:nth-child(2) > .card-app:nth-child(1) > .label-text:nth-child(2) > .label-text__content");
    public By bankAddressLabel = By.cssSelector(".client-card-block__label_add-margin > .label-text__content");
    public By bankBicLabel = By.cssSelector(".col-xl-4 .label-text:nth-child(5) > .label-text__content");
    public By bankCheckAccountLabel = By.cssSelector(".col-xl-4 .label-text:nth-child(6) > .label-text__content");
    public By bankCorrAccountLabel = By.cssSelector(".card-app:nth-child(1) > .label-text:nth-child(7) > .label-text__content");
    public By editNameRepresentativeButton = By.cssSelector(".col-md-8:nth-child(3) > .card-app:nth-child(2) .nmx-icon");
    public By applyNameRepresentativeButton = By.cssSelector(".card-app__icon-check");
    public By requiredFieldForNameRepresentativeLabel = By.cssSelector(".nm-input-v2 > .error-tooltip");
    public By requiredFieldForBasedOnLabel = By.cssSelector(".nm-textarea-v2 > .error-tooltip");
    public By nameRepresentativeInput = By.xpath("//input[@name='representativeNameGenitive']");
    public By basedOnInput = By.xpath("//input[@name='rightToSignDocumentDetails']");
    public By nameRepresentativeLabel = By.cssSelector(".card-app:nth-child(2) > .mt-15 > .label-text__content");
    public By basedOnLabel = By.cssSelector(".col-md-8:nth-child(3) > .card-app:nth-child(2) > .label-text:nth-child(3) > .label-text__content");


    public By getCompanyName(String companyName) {
        return By.linkText(companyName);
    }

}
