package Forms;

import org.openqa.selenium.By;

public class AuthorizationForm {
    public By loginInputIn = By.xpath("//input[@name='login']");
    public By passwordInputIn = By.xpath("//input[@name='password']");
    public By signInButton = By.xpath("//*[text()='Войти']");
}