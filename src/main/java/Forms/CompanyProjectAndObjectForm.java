package Forms;

import org.openqa.selenium.By;

public class CompanyProjectAndObjectForm {
    public By companyProjectAndObjectButton = By.xpath("//*[text()='Проекты и объекты']");
    public By nameCompanyProjectIn = By.xpath("//input[@name='name']");
    public By statusCompanyProjectFilter = By.cssSelector(".nm-dropdown-v2__text");
    public By statusCompanyProjectFilterLabel = By.cssSelector(".nm-dropdown-v2__item_selected > .nm-dropdown-v2__item-content");
    public By searchButton = By.xpath("//*[text()='Найти']");
    public By clearButton = By.xpath("//*[text()='Очистить']");
    public By nameCompanyProjectLabel = By.cssSelector(".row:nth-child(2) > .column:nth-child(1)");
    public By statusCompanyProjectLabel = By.cssSelector(".row:nth-child(2) > .column:nth-child(4)");
}
