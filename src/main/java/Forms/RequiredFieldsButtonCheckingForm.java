package Forms;

import org.openqa.selenium.By;

public class RequiredFieldsButtonCheckingForm {
    public By companyCreateButton = By.xpath("//*[text()='Создать компанию']");
    public By companyAddButton = By.xpath("//*[text()='Добавить']");
    public By requiredFieldForCompanyNameLabel = By.cssSelector(".client-new__row:nth-child(2) .error-tooltip");
    public By requiredFieldForShortCompanyNameLabel = By.cssSelector(".client-new__row:nth-child(3) .error-tooltip");
    public By requiredFieldForINNLabel = By.cssSelector(".client-new__row:nth-child(5) .error-tooltip");
    public By requiredFieldForAddressLabel = By.cssSelector(".client-new__row:nth-child(6) .error-tooltip");
    public By requiredFieldForCategoryLabel = By.cssSelector(".client-new__row:nth-child(10) .error-tooltip");
}
