package Forms;

import org.openqa.selenium.By;

public class CompanyFormCreateForm {
    public By businessRegistrationLabel = By.cssSelector(".nm-dropdown-v2__text_active");
    public By companyAddButton = By.xpath("//*[text()='Добавить']");
    public By fullCompanyNameInput = By.xpath("//input[@name='fullName']");
    public By companyNameInput = By.xpath("//input[@name='name']");
    public By innInput = By.xpath("//input[@name='inn']");
    public By companyAddressInput = By.cssSelector(".react-dadata__input");
    public By categoryIdLabel = By.cssSelector("#categoryId > .nm-dropdown-v2__content");
    public By categoryAddButton = By.cssSelector(".nm-dropdown-v2__item:nth-child(1) > .nm-dropdown-v2__item-content");
    public By commissionInput = By.xpath("//input[@name='ordersLimit']");
    public By companyCreateButton = By.xpath("//*[text()='Добавить']");
}
