package Forms;

import org.openqa.selenium.By;

public class CompanySearchForm {
    public By nameSubstringFilter = By.cssSelector(".nm-dropdown-v2__content");
    public By nameSubstringFilterInput = By.xpath("//input[@name='clientId']");
    public By nameEmployeeSubstringFilter = By.xpath("//input[@name='clientUserFioFilter']");
    public By phoneEmployeeSubstringFilter = By.xpath("//input[@name='clientUserPhoneFilter']");
    public By searchButton = By.xpath("//*[text()='Найти']");
    public By clearButton = By.xpath("//*[text()='Очистить']");

    public By getCompanyName(String companyName) {
        return By.linkText(companyName);
    }
}
