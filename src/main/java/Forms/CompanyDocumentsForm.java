package Forms;

import org.openqa.selenium.By;

public class CompanyDocumentsForm {
    public By documentsButton = By.xpath("//*[text()='Документы']");
    public By agencyAgreementButton = By.xpath("//*[text()='Агентский договор']");
    public By editButton = By.cssSelector(".nmx-icon");
    public By numberAgencyAgreementInput = By.cssSelector(".nm-input-v2__control");
    public By dateAgencyAgreementInput = By.cssSelector(".nm-datepicker-input__input");
    public By updateInformationButton = By.cssSelector(".card-app__icon-check");
    public By updateDateButton = By.cssSelector(".react-datepicker__day--001");
    public By numberAgencyAgreementLabel = By.cssSelector(".label-text:nth-child(2) > .label-text__content");
    public By dateAgencyAgreementLabel = By.cssSelector(".label-text:nth-child(3) > .label-text__content");

}
