package Utils;

import Models.CompanyManagerModel;

public class CompanyManagerUtil {
    public CompanyManagerModel editCompanyManager(){
        return new CompanyManagerModel()
                .setNameManager("Владимир Владимирович Владимиров")
                .setManagerPhone("9999999999")
                .setUpdateManagerPhone("+7 (999) 999-99-99");
    }
}
