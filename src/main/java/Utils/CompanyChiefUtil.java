package Utils;

import Models.CompanyChiefModel;

public class CompanyChiefUtil {
    public CompanyChiefModel editCompanyChief(){
        return new CompanyChiefModel()
                .setNameChief("Владимир Владимирович Владимиров")
                .setChiefPhone("9999999999")
                .setUpdatePhone("+7 (999) 999-99-99");
    }
}
