package Utils;

import Models.CompanyBankRepresentativeModel;

public class CompanyBankRepresentativeUtil {
    public CompanyBankRepresentativeModel editCompanyBankRepresentative(){
        return new CompanyBankRepresentativeModel()
                .setName("Владимир Владимирович Владимиров")
                .setBasedOn("на основании");
    }
}
