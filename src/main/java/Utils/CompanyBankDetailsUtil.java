package Utils;

import Models.CompanyBankDetailsModel;

public class CompanyBankDetailsUtil {
    public CompanyBankDetailsModel editCompanyBankDetails(){
        return new CompanyBankDetailsModel()
                .setBankName( "Тестовый банк")
                .setBankAddress("Кемеровская область - Кузбасс, г Анжеро-Судженск, ул Прокопьевская")
                .setBankBic("044525225")
                .setBankCheckAccount("40702810638050013199")
                .setBankCorrAccount("30101810400000000225");
    }
}
