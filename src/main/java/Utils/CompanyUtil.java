package Utils;

import Models.CompanyModel;

import java.util.UUID;

public class CompanyUtil {
    public CompanyModel createCompany(){
        return new CompanyModel()
                .setName("компания" + UUID.randomUUID())
                .setFullName("ООО ТЕСТ")
                .setAddress("394036, ОБЛ. ВОРОНЕЖСКАЯ, Г. Воронеж, УЛ. СРЕДНЕ-МОСКОВСКАЯ, Д. 7/9, ПОМЕЩ. 1/1")
                .setInn("3664069397")
                .setCommission("123");
    }
}
