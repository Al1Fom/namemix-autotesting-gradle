package Utils;

import Models.EmployeeModel;

import java.util.UUID;

public class EmployeeUtil {
    public EmployeeModel createEmployee(){
        return new EmployeeModel()
                .setLastNameEmployees("Иванов")
                .setFirstNameEmployees("Иван")
                .setPatronymicEmployees("Иванович")
                .setSnilsEmployees("12345678901")
                .setInnEmployees("520205004556")
                .setPhoneEmployees("9607777777")
                .setPhoneEmployeesLabel("+7 (960) 777-77-77")
                .setEmailEmployees("email"+ UUID.randomUUID() +"@email.com")
                .setPasswordEmployees("Qqqqq1")
                .setRepeatPasswordEmployees("Qqqqq1")
                .setPositionEmployees("Руководитель");
    }
}
