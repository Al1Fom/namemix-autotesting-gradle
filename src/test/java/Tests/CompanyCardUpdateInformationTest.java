package Tests;

import Models.CompanyModel;
import Steps.*;
import Utils.CompanyUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanyCardUpdateInformationTest {

    private MainPageSteps mainPageSteps = new MainPageSteps();
    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();
    private CompanySteps companySteps = new CompanySteps();

    @BeforeClass
    public void setUp() {
        CompanyModel companyModel = new CompanyUtil().createCompany();
        mainPageSteps.openMainPage();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
    }

    @Test(groups = {"smoke"}, description = "Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
    public void companyCardUpdateInformation() {
        companySteps.updateInformationCompanyCardCheckRequiredFields();
        companySteps.updateInformationCompanyNameCard();
        companySteps.updateInformationCompanyCard();
        companySteps.cancelUpdateInformationCompanyCard();
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }
}
