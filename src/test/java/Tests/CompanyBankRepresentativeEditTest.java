package Tests;

import Models.CompanyBankRepresentativeModel;
import Models.CompanyModel;
import Steps.*;
import Utils.CompanyUtil;
import Utils.CompanyBankRepresentativeUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanyBankRepresentativeEditTest {

    private MainPageSteps mainPageSteps = new MainPageSteps();
    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();
    private CompanySteps companySteps = new CompanySteps();

    @BeforeClass
    public void setUp() {
        CompanyModel companyModel = new CompanyUtil().createCompany();
        mainPageSteps.openMainPage();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
    }

    @Test(groups = {"smoke"}, description = "Менеджер Наймикса/Компании/Карточка компании ЮЛ/Информация/Представитель - редактирование")
    public void companyBankRepresentativeEdit() {
        CompanyBankRepresentativeModel companyBankRepresentativeEditTest = new CompanyBankRepresentativeUtil().editCompanyBankRepresentative();
        companySteps.checkingRequiredFieldCompanyBankRepresentative();
        companySteps.editCompanyBankRepresentative(companyBankRepresentativeEditTest);
        companySteps.checkCompanyBankRepresentative(companyBankRepresentativeEditTest);
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }
}
