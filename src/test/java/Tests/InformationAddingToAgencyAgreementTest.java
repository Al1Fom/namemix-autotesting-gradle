package Tests;

import Models.CompanyModel;
import Steps.*;
import Utils.CompanyUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class InformationAddingToAgencyAgreementTest {

    private MainPageSteps mainPageSteps = new MainPageSteps();
    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private CompanySteps сompanySteps = new CompanySteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();

    @BeforeClass
    public void setUp() {
        CompanyModel companyModel = new CompanyUtil().createCompany();
        mainPageSteps.openMainPage();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
    }

    @Test(groups = {"smoke"}, description = "АДМ Наймикс/Агентский договор/Добавление информации")
    public void informationAddingToDocuments() {
        сompanySteps.addingInformationToAgencyAgreement();
        сompanySteps.checkingAddedInformation();
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }
}
