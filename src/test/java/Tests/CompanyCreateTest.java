package Tests;

import Models.CompanyModel;
import Steps.*;
import Utils.CompanyUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanyCreateTest {

    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();
    private MainPageSteps mainPageSteps = new MainPageSteps();
    private CompanySteps companyParamsValidate = new CompanySteps();

    @BeforeClass
    public void setUp() {
        mainPageSteps.openMainPage();
    }

    @Test(groups = {"CompanyCreateTest.smoke"}, description = "АдминНаймикса/Компании/Добавить компанию ЮЛ - кнопка 'добавить'")
    public void createCompanyEntity() {
        CompanyModel companyModel = new CompanyUtil().createCompany();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        requiredFieldsButtonCheckingSteps.checkingRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
        companyParamsValidate.validateParamsCompany(companyModel , createCompanyYL.addCategory);
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }

}
