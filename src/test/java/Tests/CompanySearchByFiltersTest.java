package Tests;

import Models.CompanyModel;
import Models.EmployeeModel;
import Steps.*;
import Utils.CompanyUtil;
import Utils.EmployeeUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanySearchByFiltersTest {

    private MainPageSteps mainPageSteps = new MainPageSteps();
    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();
    private CompanySteps companySteps = new CompanySteps();
    private CompanyModel companyModel = new CompanyUtil().createCompany();
    private EmployeeModel employeeModel = new EmployeeUtil().createEmployee();


    @BeforeClass
    public void setUp() {
        mainPageSteps.openMainPage();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
        companySteps.addEmployees(employeeModel);
        mainPageSteps.openMainPage();
        login.authorization();
    }

    @Test(groups = "smoke", description = "АдминНаймикса/Компании/Фильтр")
    public void companySearchByFilters() {
        companySearch.searchCompanyByName(companyModel);
        companySearch.searchCompanyByNameEmployee(companyModel, employeeModel);
        companySearch.clearFilter();
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }
}
