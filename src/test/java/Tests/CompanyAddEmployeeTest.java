package Tests;

import Models.CompanyModel;
import Models.EmployeeModel;
import Steps.*;
import Utils.CompanyUtil;
import Utils.EmployeeUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanyAddEmployeeTest {

    private MainPageSteps mainPageSteps = new MainPageSteps();
    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();
    private CompanySteps companySteps = new CompanySteps();


    @BeforeClass
    public void setUp() {
        CompanyModel companyModel = new CompanyUtil().createCompany();
        mainPageSteps.openMainPage();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
    }

    @Test(groups = {"smoke"}, description = "WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - добавление")
    public void companyAddEmployee() {
        EmployeeModel employeeModel = new EmployeeUtil().createEmployee();
        companySteps.addEmployees(employeeModel);
        companySteps.checkAddEmployees(employeeModel);
        companySteps.deleteEmployees();
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }
}
