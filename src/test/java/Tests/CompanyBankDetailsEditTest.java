package Tests;

import Models.CompanyBankDetailsModel;
import Models.CompanyModel;
import Steps.*;
import Utils.CompanyUtil;
import Utils.CompanyBankDetailsUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanyBankDetailsEditTest {

    private MainPageSteps mainPageSteps = new MainPageSteps();
    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();
    private CompanySteps companySteps = new CompanySteps();

    @BeforeClass
    public void setUp() {
        CompanyModel companyModel = new CompanyUtil().createCompany();
        mainPageSteps.openMainPage();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
    }

    @Test(groups = {"smoke"}, description = "Менеджер Наймикса/Компании/Карточка компании ЮЛ/Информация/Банковские реквизиты- редактирование")
    public void companyBankDetailsEdit() {
        CompanyBankDetailsModel companyBankDetailsModel = new CompanyBankDetailsUtil().editCompanyBankDetails();
        companySteps.editCompanyBankDetails(companyBankDetailsModel);
        companySteps.checkCompanyBankDetails(companyBankDetailsModel);
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }

}
